import websocket
import requests
import json
from warnings import warn
from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session

from db import get_conn

#  Базовый класс для декларативного объявления сущностей -> объектов в БД.
Base = declarative_base()

#  Модель данных, использующаяся для хранения в БД данных о постах из ВК
class VkPost(Base):
    __tablename__ = 'vk'
    id = Column(Integer, primary_key=True)  # Публичный ключ поста в БД
    author = Column(VARCHAR(length=64))  # Ссылка на автора поста (человек/сообщество)
    creation_time = Column(DateTime)  # Дата/Время публикации поста
    event_type = Column(VARCHAR(10))  # Тип поста (новость/репост)
    event_url = Column(VARCHAR(100))  # Ссылка на пост
    text = Column(String)  # Содержание поста
    photos = Column(String)  # Ссылки на прикрепленные к посту материалы. Сериализуются в строку с разделителем ";"
    links = Column(String)  # Внешние ссылки (если пост по сути ссылка на внешний сайт)

    #  Конструктор для каждого отдельного поста
    def __init__(self, message=None):
        super().__init__()
        if not message:
            warn('Вы забыли подать сведения о посте в конструктор записи в БД. Запишите сведения вручную, '
                 'либо же заполните их из JSON-словаря при помощи функции from_json.')
        else:
            self.from_json(message)

    def __repr__(self):
        return "<Message('URL: %s','CREATED:%s', 'AUTOR: %s')>" % (self.event_url, self.creation_time, self.author)

    #  Преобразовывает приходящий json в экземпляр модели поста
    def from_json(self, message):
        msg = json.loads(message)['event'] if isinstance(message, str) else message['event']
        self.author = msg['author']['author_url']
        self.creation_time = datetime.fromtimestamp(msg['creation_time']) if isinstance(msg['creation_time'], int) \
            else msg['creation_time']
        self.event_type = msg['event_type']
        self.event_url = msg['event_url']
        links, photos = [], []
        if 'attachments' in msg.keys():
            attachments = msg['attachments']
            for att in attachments:
                if 'link' in att.keys():
                    links.append(att['link']['url'])
                if 'photo' in att.keys():
                    for key in att['photo']:
                        if key.startswith('photo'):
                            photos.append(att['photo'][key])
        self.links = ';'.join(links)
        self.photos = ';'.join(photos)
        self.text = msg['text']



#  Сборщик данных из ВК. На данный момент публичным API не обладает, функционал - только сбор данных в базу.
class VkGrabber():

    def __init__(self):
        with open('config.json', 'r') as c:
            configs = c.read()
            self.configs = json.loads(configs)
        self.token = self.configs['vk_streamer']['token']

    #  Функция авторизуется в VK streaming API.
    def get_streaming_server_key(self, token=None):
        if not token:
            token = self.token
        request_url = "https://api.vk.com/method/streaming.getServerUrl?access_token={}&v=5.64".format(token)
        r = requests.get(request_url)
        data = r.json()
        self.stream = {"server": data["response"]["endpoint"], "key": data["response"]["key"]}
        return self.stream

    #  Функция запускает websocket приложение, которое слушает новостной поток ВК по тегам (задать теги можно при помощи
    #  set_rules(rules)
    def listen_stream(self, stream=None):
        if not stream:
            stream = self.stream
        websocket.enableTrace(True)
        ws = websocket.WebSocketApp("wss://{}/stream?key={} ".format(stream["server"], stream["key"]),
                                    on_message=self.on_message,
                                    on_error=self.on_error,
                                    on_close=self.on_close)
        ws.on_open = self.on_open
        while True:
            ws.run_forever()

    #  Устанавливает список тегов, по которому отлавливаются посты
    def set_rules(self, rules):
        # rule_params = {"rule": {"value": rules, "tag": 'tag_' + str(random.randint(11111, 99999))}}
        rule_params = {'rule': {'value': rules, 'tag': 'sberbank'}}
        headers = {'content-type': 'application/json'}
        r = requests.post("https://{}/rules?key={}".format(self.stream["server"], self.stream["key"]),
                          data=json.dumps(rule_params), headers=headers)
        data = r.json()
        return data['code'] == 200

    #  Получает список тегов, по которому отлавливаются посты
    def get_rules(self):
        r = requests.get("https://{}/rules?key={}".format(self.stream["server"], self.stream["key"]))
        data = r.json()
        if data['code'] != 200:
            return False
        return data['rules']

    #  Функция вызывается при получении сообщения. Это точка входа в обработку постов, любая предобработка
    #  должна быть ЗДЕСЬ
    def on_message(ws, message):
        jsoned = json.loads(message)
        record = VkPost(jsoned)
        print('>>>> receive message: ', jsoned)
        session = sessionmaker(bind=ws.connection)
        session = session()
        try:
            session.add(record)
            session.commit()
        except Exception as e:
            session.rollback()
        session.close()


    #  Функция вызывается при ошибке в работе сборщика данных. Обработка исключений должна быть ЗДЕСЬ
    def on_error(ws, error):
        print(">>>> error thead:", error)

    #  Функция вызывается перед окончанием работы сборщика данных. Здесь, например, закрывается сессия с БД.
    def on_close(ws):
        print(">>>> close thead")

    #  Функция вызывается перед началом работы сборщика данных. Здесь, например, открывается сессия с БД.
    def on_open(ws):
        if not hasattr(ws, 'connection'):
            ws.connection = get_conn()
        print(">>>> open thead")



if __name__ == '__main__':
    conn = get_conn()
    #  Если в базе из config.json нет таблички для вконтакте, она автоматически создастся
    if not conn.dialect.has_table(conn, 'vk'):
        print('Создаем таблицу для vk...')
        Base.metadata.create_all(conn)
    grabber = VkGrabber()
    key = grabber.get_streaming_server_key()
    grabber.set_rules(['сбербанк', 'sber', 'сбер', 'sberbank'])
    grabber.listen_stream()