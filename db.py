#  Запустите этот файл отдельно, если вам нужно поставить копию проекта с нуля, или привести базу данных к актуальному виду.
#  Вызовите get_conn(), если вам нужно присоединиться к базе данных.

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import inspect
import json

#  Устанавливает связь с БД
def get_conn():
    with open('config.json', 'r') as c:
        config = json.loads(c.read())
    conn_string = 'postgresql+psycopg2://{user}:{password}@{host}/{dbname}'.format(**config['db'])
    return create_engine(conn_string)

#  Открывает сессию с БД
def open_session():
    session = sessionmaker(bind=get_conn())
    return session()

#  Представляет запись БД как питон-словарь
def to_dict(obj):
    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}

#  Удаляет таблицу, в которую отображается модель данных. На начальном этапе проекта, тем более без поддержки миграций,
#  используйте эту функцию, чтобы снести то, что вы наворотили в базе данных
def drop_table(Model):
    Model.__table__.drop(get_conn())
