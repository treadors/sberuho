﻿import snowballstemmer as sbs
import sqlite3
import config
import datetime
import re
from sklearn.externals import joblib


class clf_model:

    def __init__(self):
        pass

    # добавление размеченного соообщения в базу
    def insert_twit_for_statistic(self, text_tw=''):
        clf = joblib.load(r'C:\Users\mironov-sa\Documents\TestMyBot\filename.pkl')

        con = sqlite3.connect(config.PATH_TO_BASE)
        cur = con.cursor()
        # message, number_week, label, date
        now = int(datetime.datetime.now().strftime('%U'))
        year = int(datetime.datetime.now().strftime('%Y'))
        cur.execute('insert into MessagesForStatistic VALUES ("{0}",{1},"{2}","{3}",{4})'\
                    .format(text_tw, now, clf.predict([text_tw])[0], datetime.datetime.now(), year))
        con.commit()

    def text_cleaner(self, text):
        text = text.lower()  # приведение в lowercase,
        text = re.sub(r'https?://[\S]+', ' url ', text)  # замена интернет ссылок
        text = re.sub(r'[\w\./]+\.[a-z]+', ' url ', text)
        # text = re.sub( r'\d+[-/\.]\d+[-/\.]\d+', ' date ', text) # замена даты и времени
        # text = re.sub( r'\d+ ?гг?', ' date ', text)
        # text = re.sub( r'\d+:\d+(:\d+)?', ' time ', text)
        text = re.sub(r'@\w+', ' tname ', text)  # замена имён twiter
        text = re.sub(r'#\w+', ' htag ', text)  # замена хештегов
        text = re.sub(r'<[^>]*>', ' ', text)  # удаление html тагов
        text = re.sub(r'[\W]+', ' ', text)  # удаление лишних символов
        stemmer = sbs.RussianStemmer()
        text = ' '.join(stemmer.stemWords(text.split()))
        stw = ['в', 'по', 'на', 'из', 'и', 'или', 'не', 'но', 'за', 'над', 'под', 'то',
               'a', 'at', 'on', 'of', 'and', 'or', 'in', 'for', 'at']
        remove = r'\b(' + '|'.join(stw) + ')\b'
        text = re.sub(remove, ' ', text)
        text = re.sub(r'\b\w\b', ' ', text)  # удаление отдельно стоящих букв
        text = re.sub(r'\b\d+\b', ' digit ', text)  # замена цифр
        return text
