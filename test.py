from sqlalchemy import inspect
from db import open_session
from vk import VkPost

def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}

session = open_session()
posts = session.query(VkPost)
for post in posts:
    print(object_as_dict(post))
print('haha')