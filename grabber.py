from abc import ABC, abstractmethod

class BaseGrabber(ABC):

    @abstractmethod
    def get_streaming_server_key(self, token=False):
        pass

    @abstractmethod
    def listen_stream(self):
        pass

    @abstractmethod
    def set_rules(self, rules):
        pass

    @abstractmethod
    def get_rules(self):
        pass

    @abstractmethod
    def on_message(ws, message):
        print('>>>> receive message: ', message)

    @abstractmethod
    def on_error(ws, error):
        print(">>>> error thead:", error)

    @abstractmethod
    def on_close(ws):
        print(">>>> close thead")

    @abstractmethod
    def on_open(ws):
        print(">>>> open thead")



