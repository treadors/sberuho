﻿import telebot
import config
import time
import SearchBotTwitter as SBT
from tweepy.error import TweepError
import threading
import pandas as pd
import clf_model as CM
import datetime
import sqlite3
import matplotlib.pyplot as plt
plt.style.use('ggplot')  # Красивые графики
plt.rcParams['figure.figsize'] = (15, 15)  # Размер картинок

bot = telebot.TeleBot(config.token)
run = False

@bot.message_handler(commands=['lastweek'])
def handle_message(message):
    # bot.send_message(message.chat.id, bot.get_me())
    #week = int(str(message).replace('\week', ''))
    week = int(datetime.datetime.now().strftime('%U'))
    path = create_weekly_report(week-1)
    with open(path, 'rb') as foto:
        try:
            #foto = open(path, 'rb')
            bot.send_photo(message.chat.id, foto)
        except Exception as e:
            print(e)

@bot.message_handler(commands=['curweek'])
def handle_message_curWeek(message):
    # bot.send_message(message.chat.id, bot.get_me())
    #week = int(str(message).replace('\week', ''))
    week = int(datetime.datetime.now().strftime('%U'))
    path = create_weekly_report(week)
    with open(path, 'rb') as foto:
        try:
            #foto = open(path, 'rb')
            bot.send_photo(message.chat.id, foto)
        except Exception as e:
            print(e)

# Обработка команд '/start' и '/help'.
@bot.message_handler(commands=['lastT'])
def handle_end_Twitt(message):
    bot.send_message(config.CHANEL_ID, botTw.endTwitt)
#    config.RUN = True

@bot.message_handler(commands=['test'])
def handle_start_help(message):
    bot.send_message(message.chat.id, 'BOT - работает')
    #config.RUN = False

"""
@bot.message_handler(func=lambda message: True, content_types=['text'])
def repeat_all_messages(message):
    #bot.send_message(message.chat.id, bot.get_me())
    path = create_weekly_report()
    try:
        foto = open(path, 'rb')
        bot.send_photo(message.chat.id, foto)
    except Exception as e:
        print(e)

# Обработчик документов и аудио
@bot.message_handler(content_types=['document', 'audio'])
def handle_docs_audio(message):
    pass


#Обработчик сообщений, подходящих под указанное регулярное выражение
@bot.message_handler(regexp="SOME_REGEXP")
def handle_message(message):
    pass

# Обработчик сообщений, содержащих документ с mime_type 'text/plain' (обычный текст)
@bot.message_handler(func=lambda message: message.document.mime_type == 'text/plain', content_types=['document'])
def handle_text_doc(message):
    pass
"""

# компоновка отчета за неделю по инцидентам
def create_weekly_report(n_week=0):
    con = sqlite3.connect(r'C:\Users\mironov-sa\Documents\TestMyBot\testTwitter.db')
    cur = con.cursor()
    year = int(datetime.datetime.now().strftime('%Y'))
    r = cur.execute('select * from MessagesForStatistic where number_week={0} and year={1}'.format(n_week, year))
    mass = []
    for i in r.fetchall():
        mass.append(i)
    cur.close()
    con.close()
    dSet = 0
    dSet = pd.DataFrame(mass, columns=['mess', 'week', 'l', 'date', 'year'])
    dSet.drop_duplicates(inplace=True)
    label=''
    data=''
    print('week: {0}\t shape: {1}'.format(n_week, dSet.shape))
    label = pd.crosstab(dSet['l'], dSet['week']).index
    data = pd.crosstab(dSet['l'], dSet['week']).values
    print('len(l): {0}\t len(d): {1}'.format(len(label), len(data)))
    c = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#17becf", "#e377c2", "#7f7f7f", "#bcbd22", "#8c564b"]
    patches, texts, autotexts = plt.pie(data, labels=label, autopct='%1.1f%%', shadow=True, colors=c, labeldistance = .9)
    patches = 0
    print('pathes: {}'.format(patches))
    for t in texts:
        t.set_size(18)
    for t in autotexts:
        t.set_size(14)
    patches, texts, autotexts = 0,0,0
    
    plt.title('Статистика за {0}-ю неделю {1} года'.format(n_week+1, year))
    plt.savefig('My_image{0}-{1}.png'.format(n_week, year))
    plt.close()
    return 'My_image{0}-{1}.png'.format(n_week, year)

# главная логика спама
def mainLogic():
    try:
        temp = botTw.endTwitt
        print("Ищу твиты")
        botTw.endTwitt = botTw.getTwitSberForward(botTw.endTwitt, 1, cursor)  # 927505835911208960
        if temp != botTw.endTwitt:
            print("Щас выведу новость")
            botTw.getChat(botTw.endTwitt, cursor=cursor)
            if (botTw.id in config.MASSIVE_OLD_ID) == False:
                config.MASSIVE_OLD_ID.append(botTw.id)
                infoMass = botTw.getInfoAboutTwitt(botTw.id, cursor)
                # кастомный текст для сообщения
                #text = 'Пользователь {0}.\nВ {1}\nОставил следующее сообщение:\n{2}\nСсылка - {3}' \
                #    .format(infoMass[0], infoMass[1], infoMass[2], infoMass[3])

                clf_m = CM.clf_model()
                clf_m.insert_twit_for_statistic(clf_m.text_cleaner(infoMass[2]))

                text = '{0}'.format(infoMass[3])
                # обычная ссылка
                bot.send_message(config.CHANEL_ID, text, disable_notification=True)
                print(text)
            else:
                print('Такой твит уже выводился')
        else:
            print('Нет новостей')
        time.sleep(40)
    except TweepError:
        time.sleep(120)

# вывод статистики за неделю
def run_statistic():
    while config.RUN:
        week = int(datetime.datetime.now().strftime('%U'))
        if config.OLD_WEEK != week:
            config.OLD_WEEK = week
            path = create_weekly_report(week-1)
            with open(path, 'rb') as foto:
                try:
                    print("Вот тебе картинка")
                    bot.send_photo(config.CHANEL_ID, foto)
                except Exception as e:
                    print(e)
        else:
            print('Не время для статы')
        time.sleep(1000)


# точка запуска потока спама
def run_mainLogig():
    while config.RUN:
        try:
            mainLogic()
        except:
            mainLogic()

def pool_bot():
    bot.polling(none_stop=True)

if __name__ == '__main__':
    print("Start")

    botTw = SBT.SearchBotTwitter()
    cursor = botTw.startBot()
    endTwit = ''

    pipe = []
    th = threading.Thread(target=run_mainLogig)
    th2 = threading.Thread(target=run_statistic)
    th3 = threading.Thread(target=pool_bot)

    pipe.append(th)
    pipe.append(th2)
    pipe.append(th3)

    for i in pipe:
        i.start()

    #bot.polling(none_stop=True)
